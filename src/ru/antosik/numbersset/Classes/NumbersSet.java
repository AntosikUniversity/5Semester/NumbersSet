package ru.antosik.numbersset.Classes;

import java.util.*;
import java.util.stream.Collectors;

public class NumbersSet {
    private List<Integer> list = new ArrayList<>();

    public NumbersSet(int... numbersArg) {
        for (int number : numbersArg)
            list.add(number);
    }

    public NumbersSet(Collection<Integer> numbersArg) {
        list.addAll(numbersArg);
    }

    @Override
    public String toString() {
        return list.toString();
    }

    public Integer[] getNumbers() {
        return list.toArray(new Integer[0]);
    }

    public static NumbersSet Union(NumbersSet first, NumbersSet second) {
        List<Integer> results = new ArrayList<>();

        results.addAll(first.list);
        results.addAll(second.list);

        return new NumbersSet(new ArrayList<>(new HashSet<>(results)));
    }

    public static NumbersSet Intersect(NumbersSet first, NumbersSet second) {
        return new NumbersSet(
                first.list
                        .stream()
                        .filter(el -> second.list.contains(el))
                        .collect(Collectors.toList()
                )
        );
    }
}
