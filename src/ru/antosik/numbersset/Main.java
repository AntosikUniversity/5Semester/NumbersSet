package ru.antosik.numbersset;

import ru.antosik.numbersset.Classes.NumbersSet;

import java.util.List;

public class Main {

    public static void main(String[] args) {
        NumbersSet first = new NumbersSet(List.of(1,2,3,4,5));
        NumbersSet second = new NumbersSet(List.of(4,5, 6,7,8));

        System.out.println(NumbersSet.Union(first, second));
        System.out.println(NumbersSet.Intersect(first, second));
    }
}
